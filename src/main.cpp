#include <Arduino.h>

#define Start 6 //  nút chạy, đảo chiều băng tải
#define Stop A1  // nút dừng băng tải
#define ledStart 9 //  trạng thái led start
#define ledStop 10  // trạng thái led stop
#define Au_Man A0  // 2 chế độ auto hoặc manual

#define RunA1 7  //chạy băng tải input
#define Reverse1 8 //đảo chiều băng tải input
#define RunA2 11  //chạy băng tải output
#define Reverse2 12 //đảo chiều băng tải output
#define sensor1 A2 //cảm biến băng tải input
#define sensor2 A3 //cảm biến băng tải output

int StartState = HIGH;  // trạng thái băng tải
int lastStartState;  // trạng thái nút Start
int currentStartState; // trạng thái hiện tại của Start

int StopState = HIGH;  // trạng thái dừng băng tải
int lastStopState;  // trạng thái nút Stop
int currentStopState; // trạng thái hiện tại của Stop

int mode;
int flag = 0;
int flag2;
int flag1; // dừng băng tải khi đảo chiều
String inputString = "";         // chuỗi lưu data nhận đến
bool stringComplete = false;  // kiểm tra chuỗi đã gửi ok 
int timer_stop = 1000;

String Th1 = "BT1!";
String Th2 = "BT2!";
String Th3 = "BT3!";
String Th4 = "BT4!";
String Th5 = "BT5!";
String Th6 = "BT6!";
String Th7 = "BT7!";
String Th8 = "BT8!";

void mannualMode();
void autoMode();
void BatBangTai();
void TatBangTai();
void Bt1_Input();
void Bt2_Input();
void Bt1_Output();
void Bt2_Output();
void Xoachuoi();
void Bt2_Stop();
void Bt1_Stop();

void setup() {
  Serial.begin(9600);
  pinMode(Start, INPUT_PULLUP);
  pinMode(Stop, INPUT_PULLUP);
  pinMode(Au_Man, INPUT_PULLUP);
  pinMode(sensor1, INPUT_PULLUP);
  pinMode(sensor2, INPUT_PULLUP);

  pinMode(ledStop, OUTPUT);
  pinMode(ledStart, OUTPUT);
  pinMode(RunA1, OUTPUT);
  pinMode(Reverse1, OUTPUT);
  pinMode(RunA2, OUTPUT);
  pinMode(Reverse2, OUTPUT);

  digitalWrite(RunA1, HIGH);   // tắt băng tải khi khởi động
  digitalWrite(Reverse1, HIGH);
  digitalWrite(RunA2, HIGH);   // tắt băng tải khi khởi động
  digitalWrite(Reverse2, HIGH);
  digitalWrite(ledStart, HIGH);  // tat led start khi khoi dong

  currentStartState = digitalRead(Start);  // đọc trạng thái ban đầu của nút Start
  delay(20);
  currentStopState = digitalRead(Stop);  // đọc trạng thái ban đầu của nút Stop
  delay(20);
}

void loop() { 
  mode = digitalRead(Au_Man);  
  switch (mode){
    case 0:                      /// chế độ auto
      //Serial.println("in auto");
      if(flag == 1){
        TatBangTai();
        digitalWrite(ledStop, HIGH);  // led stop tat
        digitalWrite(ledStart, HIGH);  // led start tắt
        inputString = ""; /// xóa chuỗi khi từ chế độ man vào auto
        flag = 0;
      }
      else{
        autoMode();
      }
      flag2 = 1;
      break;

    case 1:                     ///chế độ manual
      // Serial.println("mannual");
      if(flag2 == 1){
        TatBangTai();
        digitalWrite(ledStop, HIGH);  // led stop tat
        digitalWrite(ledStart, HIGH);  // led start tắt
        flag2 = 0;
      }
      else{
      mannualMode();
      }
      flag = 1;
      break;        
    }
}

void serialEvent(){
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    inputString += inChar;  // gắn từng ký tự nhận dc vào inputString
    if (inChar == '!') {  // gặp dấu ! thì chuỗi gửi
      stringComplete = true;
    }
  }
}

void mannualMode(){
  lastStartState = currentStartState;  //lưu trạng thái của start
  currentStartState = digitalRead(Start); // đọc trạng thái mới của start
  delay(20);

  lastStopState = currentStopState;  //lưu trạng thái của start
  currentStopState = digitalRead(Stop); // đọc trạng thái mới của stop
  delay(20);

  if(lastStartState == 1 && currentStartState == 0){ // bật tắt băng tải 
    StartState =! StartState;
    BatBangTai();
    flag1 = 1;
    if(flag1 == 1){
      TatBangTai();
      delay(500);
      flag1 = 0;
    }
    BatBangTai();
    digitalWrite(Reverse1, StartState);
    digitalWrite(Reverse2, StartState);
    digitalWrite(ledStop, HIGH);  // led stop tat
    digitalWrite(ledStart, LOW);  // led start sáng
  }
  if(lastStopState == 1 && currentStopState == 0){ // dừng băng tải 
    StopState =! StopState;
    TatBangTai();
    digitalWrite(ledStop, LOW);  // led stop sáng
    digitalWrite(ledStart, HIGH);  // led start tat
  } 
}

void autoMode()
{
  if (inputString == Th1)
    {
        bool detect1 = false;
        bool detect2 = false;
        int cout1 = 0;
        int cout2 = 0;

        Bt1_Input();
        Bt2_Input();

        while (true)
        {
            int ss1 = digitalRead(sensor1);
            int ss2 = digitalRead(sensor2);

            if(ss1 == 0)    detect1 = true;
            if(ss2 == 0)    detect2 = true;

            if(ss1 == 1 && detect1 == true )
            {
                cout1++;
                delay(1);
                if(cout1 > timer_stop)
                {
                    Bt1_Stop();
                    detect1 = false;    
                }
            }

            if(ss2 == 1 && detect2 == true)
            {
                cout2++;
                delay(1);
                if(cout2 > timer_stop)
                {
                    Bt2_Stop();
                    detect2 = false;   
                }
            }

            if(cout1 > timer_stop && cout2 > timer_stop)
            {
                Serial.println("BT1OK!");
                break;
            }

        }
    }

  else if (inputString == Th2)
    {
        bool dectect_1 = false;
        bool dectect_2 = false;
        int cout1 = 0;
        int cout2 = 0;

        Bt1_Input();
        Bt2_Output();

        while (true)
        {
            int ss1 = digitalRead(sensor1);
            int ss2 = digitalRead(sensor2);

            if (ss1 == 0)   dectect_1 = true;
            if (ss2 == 0)   dectect_2 = true;

            if (ss1 == 1 && dectect_1 == true)
            {
                cout1 ++;
                delay(1);
                if (cout1 > timer_stop)
                {
                    Bt1_Stop();
                    dectect_1 = false;
                }
            }

            if (ss2 == 1 && dectect_2 == true)
            {
                cout2 ++;
                delay(1);
                if (cout2 > timer_stop)
                {
                    Bt2_Stop();
                    dectect_2 = false;
                }
            }
            
            if (cout1 > timer_stop && cout2 > timer_stop)
            {
                Serial.println("BT2OK!");
                break;
            }
        }
    }  

  else if (inputString == Th3)
  {
    bool dectect_1 = false;
    bool dectect_2 = false;
    int cout1 = 0;
    int cout2 = 0;

    Bt1_Output();
    Bt2_Output();

    while (true)
    {
        int ss1 = digitalRead(sensor1);
        int ss2 = digitalRead(sensor2);

        if (ss1 == 0)   dectect_1 = true;
        if (ss2 == 0)   dectect_2 = true;

        if (ss1 == 1 && dectect_1 == true)
        {
            cout1 ++;
            delay(1);
            if (cout1 > timer_stop)
            {
                Bt1_Stop();
                dectect_1 = false;
            }
        }

        if (ss2 == 1 && dectect_2 == true)
        {
            cout2 ++;
            delay(1);
            if (cout2 > timer_stop)
            {
                Bt2_Stop();
                dectect_2 = false;
            }
        }
        
        if (cout1 > timer_stop && cout2 > timer_stop)
        {
            Serial.println("BT3OK!");
            break;
        }
    }

  }

  else if (inputString == Th4)
    {
        bool dectect_1 = false;
        bool dectect_2 = false;
        int cout1 = 0;
        int cout2 = 0;

        Bt1_Output();
        Bt2_Input();

        while (true)
        {
            int ss1 = digitalRead(sensor1);
            int ss2 = digitalRead(sensor2);

            if (ss1 == 0)   dectect_1 = true;
            if (ss2 == 0)   dectect_2 = true;

            if (ss1 == 1 && dectect_1 == true)
            {
                cout1 ++;
                delay(1);
                if (cout1 > timer_stop)
                {
                    Bt1_Stop();
                    dectect_1 = false;
                }
            }

            if (ss2 == 1 && dectect_2 == true)
            {
                cout2 ++;
                delay(1);
                if (cout2 > timer_stop)
                {
                    Bt2_Stop();
                    dectect_2 = false;
                }
            }
            
            if (cout1 > timer_stop && cout2 > timer_stop)
            {
                Serial.println("BT4OK!");
                break;
            }
        }
    }

  else if (inputString == Th5)
      {
      Bt1_Input();
      Bt2_Stop();
      while (1)
      {
        int ss1 = digitalRead(sensor1);
        delay(10);
        if(ss1 == 0){
          while (1)
          {
            int ss1 = digitalRead(sensor1);
            if(ss1 == 1){      /// bắt sườn lên của cảm biến tiệm cận từ 0 -> 1 thì dừng bt
              delay(1000);
              Bt1_Stop();
              Serial.println("BT5OK!");
              break;
            }
          }
          break;  /// thoát khỏi th5
        }
        else{
          Bt1_Input();
          Bt2_Stop();
        }
      }
    }
  
  else if (inputString == Th6)
    {
        bool detect = false;
        int cout = 0;
        Bt1_Output();
        Bt2_Stop();
        while (1)
        {
            int ss1 = digitalRead(sensor1);
            if(ss1 == 0)    detect = true;

            if(detect == true && ss1 == 1)
            {
                cout++;
                delay(1);
                if(cout > timer_stop)
                {
                    Bt1_Stop();
                    detect = false;
                }
            }

            if(cout > timer_stop)
            {
                Serial.println("BT8OK!");
                break;
            }
        }
    }

  else if (inputString == Th7)
    {
        bool detect = false;
        int cout = 0;
        Bt1_Stop();
        Bt2_Output();
        while (1)
        {
            int ss2 = digitalRead(sensor2);
            if(ss2 == 0)    detect = true;

            if(detect == true && ss2 == 1)
            {
                cout++;
                delay(1);
                if(cout > timer_stop)
                {
                    Bt2_Stop();
                    detect = false;
                }
            }

            if(cout > timer_stop)
            {
                Serial.println("BT7OK!");
                break;
            }
        }
    }

  else if (inputString == Th8)
    {
        bool detect = false;
        int cout = 0;
        Bt1_Stop();
        Bt2_Input();
        while (1)
        {
            int ss2 = digitalRead(sensor2);
            if(ss2 == 0)    detect = true;

            if(detect == true && ss2 == 1)
            {
                cout++;
                delay(1);
                if(cout > timer_stop)
                {
                    Bt2_Stop();
                    detect = false;
                }
            }

            if(cout > timer_stop)
            {
                Serial.println("BT8OK!");
                break;
            }
        }
    }
  
  else{}

  Xoachuoi();

}

void TatBangTai(){
  digitalWrite(RunA1, HIGH);
  digitalWrite(RunA2, HIGH);
}

void BatBangTai(){
  digitalWrite(RunA1, LOW);
  digitalWrite(RunA2, LOW);
}

void Bt1_Input(){
  digitalWrite(RunA1, LOW);
  digitalWrite(Reverse1, 0);
}

void Bt2_Input(){
  digitalWrite(RunA2, LOW);
  digitalWrite(Reverse2, 0);
}

void Bt1_Output(){
  digitalWrite(RunA1, LOW);
  digitalWrite(Reverse1, 1);
}

void Bt2_Output(){
  digitalWrite(RunA2, LOW);
  digitalWrite(Reverse2, 1);
}

void Bt1_Stop(){
  digitalWrite(RunA1, HIGH);
}

void Bt2_Stop(){
  digitalWrite(RunA2, HIGH);
}

void Xoachuoi(){
  if (stringComplete){
    inputString = ""; //clear
    stringComplete = false;
  }
}
